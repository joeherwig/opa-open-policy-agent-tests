package petCare.rules
import data
import input

allowedToRide = true {
	input.family == "herwig"
	input.animal == "lena"
}

allowedToStayAloneWithAnimals = true {
	# only people being at least 10 years should stay alone with the animal.
	some dog
	some familyMember
	lower(input.user) == lower(data.familyMembers[familyMember].name)
	time.parse_rfc3339_ns(data.familyMembers[familyMember].yearOfBirth) < time.add_date(time.now_ns(), -10, 0, 0)
	# lower(input.animal) == lower(data.animals[dog].name)
	"dog" == data.animals[dog].type
}

allowedToWalkTheDog = true {
	# only people being at least 16 years old are allowed to walk with the dog.
	some dog
	some familyMember
	lower(input.user) == lower(data.familyMembers[familyMember].name)
	time.parse_rfc3339_ns(data.familyMembers[familyMember].yearOfBirth) < time.add_date(time.now_ns(), -16, 0, 0)
	# lower(input.animal) == lower(data.animals[dog].name)
	"dog" == data.animals[dog].type
}

SQLQueryToProcess = x {
	# only people being at least 16 years old are allowed to walk with the dog.
	some dog
	some familyMember
	lower(input.user) == lower(data.familyMembers[familyMember].name)
	#time.parse_rfc3339_ns(data.familyMembers[familyMember].yearOfBirth) < time.add_date(time.now_ns(), -16, 0, 0)
	lower(input.animal) == lower(data.animals[dog].name)
	#"dog" == data.animals[dog].type
	#x := concat(" ", ["Select * from bla where", data.animals[dog].name])
	x := "Select"
}
