# open policy agent scripts

Main purpose is to get used to the OPA approaches.

#Prerequesities

- [VSCode](https://code.visualstudio.com/)
- [Open Policy Agent Extension](https://marketplace.visualstudio.com/items?itemName=tsandall.opa&ssr=false#overview)

Open one of the contained folders in VSCode, go to the included `.rego` file and run within the command palette the `OPA Evaluate Package`.