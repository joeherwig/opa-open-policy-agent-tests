package petCare.rules
import data
import input

allowedToRide = true {
	input.family == "herwig"
	input.animal == "lena"
}

#allow{
#	allowedDocumentIDs[document]
#	data.document.creator = input.user
#}

allowedDocumentIDs[document.id]  {
	# only people being at least 10 years should stay alone with the animal.
	data.documents[_]
	document.creator = input.user
	document = data.documents[_]
	# time.parse_rfc3339_ns(data.documents[document].createdDateTime) < time.add_date(time.now_ns(), -10, 0, 0)
}

allowedDocumentIDs[document.id]  {
	# only people being at least 10 years should stay alone with the animal.
	data.documents[_]
	document.delegates[_] = input.user
	document = data.documents[_]
	# time.parse_rfc3339_ns(data.documents[document].createdDateTime) < time.add_date(time.now_ns(), -10, 0, 0)
}

allowedDocumentIDs["SELECT blob FROM document WHERE creator = "]  {
	# only people being at least 10 years should stay alone with the animal.
	data.documents[_]
	document.creator = input.user
	document = data.documents[_]
	# time.parse_rfc3339_ns(data.documents[document].createdDateTime) < time.add_date(time.now_ns(), -10, 0, 0)
}